import React from "react";
import { Route, Routes} from "react-router-dom";
import OverViewMain from "./components/OverViewMain"
import SignIn from "./auth/SignIn"
import LogIn from "./auth/LogIn";
import OverViewPage from "./components/OverViewPage";
import { QueryClientProvider, QueryClient } from "react-query";


function App() {
  return (
    <div className="App">
    <Routes>
      <Route path='/' element={<OverViewMain/>}/>
      <Route path='/LogIn' element={<LogIn/>}/>
      <Route path='/SignIn' element={<SignIn/>}/>
      <Route path='/OverViewPage' element={<OverViewPage/>}/>
    </Routes>
    </div>
  );
}

export default App;