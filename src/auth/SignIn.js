import React, {useState} from "react";
import {createUserWithEmailAndPassword} from "firebase/auth";
import {auth} from "../firebase"
import {useNavigate} from "react-router-dom";
import {Link} from "react-router-dom";

const Register = () => {

  const [email , setEmail] = useState('')
  const [password , setPassword] = useState('')
  const navigate = useNavigate()
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      navigate("/")
    } catch (err) {
      console.log(err)
      alert('Royxatdan oting')
    }

  };

  return (
    <div className="formContainer bg-[#a7bcff] h-screen flex items-center justify-center">
      <div className="formWrapper bg-white py-[20px] px-[60px] rounded-[10px] flex flex-col gap-[10px] items-center">
        <span className="text-[#5d5b8d] font-bold text-[24px]">Porfume</span>
        <span className="text-[#5d5b8d] text-xl font-bold">Register</span>
        <form onSubmit={handleSubmit} className="flex flex-col gap-[15px]">
          <input className="p-[15px] border-solid border-[2px] outline-none rounded-md w-64 border-b-[#a7bcff]"
                 type="text" placeholder="Your name"/>
          <input
            className="p-[15px] border-solid border-[2px] outline-none rounded-md w-64 border-b-[#a7bcff]"
            type="email"
            placeholder="Your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <input
            className="p-[15px] border-solid border-[2px] outline-none rounded-md w-64 border-b-[#a7bcff]"
            type="password"
            placeholder="Your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <button className="bg-[#7b96ec] text-white p-[10px] font-bold border-none cursor-pointer">Sign Up</button>
        </form>
        <p className="text-[#5d5b8d] text-12px mt-2">You do have an account?
          <Link to={'/login'} className='text-blue-700'>Login</Link>
        </p>
      </div>
    </div>
  )
}
export default Register;
