import React, { useContext, useEffect, useMemo, useState,} from "react";
import { Context } from "../context";
import { useNavigate } from "react-router-dom";

function OverViewCars() {
    const {state, dispatch} = useContext(Context)
    const [item, setItem] = useState(state.data)
    const navigate = useNavigate()
  

  
    const Search = useMemo(() => {
      return state.data.filter((post) => post.name.toLowerCase().includes(state.search) &&
        post.category.toLowerCase().includes(state.filter))
    }, [state.search, state.filter])

    const product = (id) => {
      dispatch({type: 'PRODUCT', payload: id})
      navigate('/OverViewPage')
    }

    useEffect(() => {
      dispatch({type: 'PRODUCT', payload: item})
    }, [item])


    return (
        <div className="w-full flex justify-center relative">
          <div className='container flex items-center flex-wrap justify-evenly'>
            {Search.map(item => (
                <div
                key={item.id}
                onClick={() => product(item.id)}
                className="2xl:w-1/4 md:w-72 bg-white border-[1.5px] border-[#eeeeee] mt-8 mb-10 flex flex-col justify-center items-start xl:mx-2 mx-4 pb-8"
              >
                <div >
                <img
                  src={item.img}
                  alt={item.name}
                  className='xl:w-96 md:h-60 bg-cover my-2 md:px-5 rounded-t-lg rounded-lg'
                />
                <div className='text-lg leading-6 px-5'>
                  <p className=" font-serif">{item.name}</p>
                  <p className="pt-1 font-serif"> {item.country}</p>
                  <p className="pt-1 font-serif">Narxi: {item.price} $</p>
                  <div className='text-white mt-4'>
                  </div>
                </div>
                </div>          
              </div>
            ))}
          </div>
        </div>
      )

}
export default OverViewCars;
