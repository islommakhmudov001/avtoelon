import React, { useEffect, useState } from "react";
import OverViewCars from "./OverViewCars"
import Header from "../container/Header"
import Footer from "../container/Footer"
import { Link } from "react-router-dom";

function Main () {
    const [theme, setTheme] = useState(null);

    useEffect(() => {
        if(window.matchMedia('(prefers-color-scheme: dark)').matches){
        setTheme('dark');
        }
    }, [])

    useEffect(() => {
        if(theme === "dark") {
            document.documentElement.classList.add("dark");
        } else {
            document.documentElement.classList.remove("dark");
        }
    }, [theme]);

    const handleThemeSwitch = () => {
        setTheme(theme === "dark" ? "light" : "dark");
    }

    return(
        <div className="bg-white dark:bg-[#292929]">
            <div className="w-full h-10 shadow-[0_0_10px_0] shadow-black mb-2">
            <div className="container h-full flex justify-end items-center">
            {/* <span className="font-medium">DarkMode</span> */}
            <label class="relative inline-flex ml-2 items-center cursor-pointer">
             <input type="checkbox" value="" class="sr-only peer" onClick={handleThemeSwitch}/>
             <div class="w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full
              peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:left-[2px]
               after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5
                after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-[#292929] border-2 border-white"></div>
            </label>

            <button>
                <Link to={'/Login'}>
                 Kirish
                </Link>
            </button>
            </div>
            </div>
           <Header/>
           <OverViewCars/>
           <Footer/>
        </div>
    )
}
export default Main