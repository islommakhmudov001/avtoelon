import React, {useContext, useState} from "react"
import { Context } from "../context"

function OverViewPage() {
  const {state, dispatch} = useContext(Context)
  const [incr, setIncr] = useState(0)

  return (
    <div className=" w-full h-screen flex items-center justify-center">

        {state.product.map(item => (
        <div className="w-full h-full flex"
             key={item.id}>
            <div className="w-1/2 h-screen">
            <div className="w-9/12 h-4/6 mt-8 mx-16 rounded-t-lg rounded-lg">
                <img
                  src={item.img}
                  alt={item.name}
                  className='w-full h-full object-cover rounded-t-lg rounded-lg'
                 />
            </div>
            <p className="text-xl font-mono pt-5 px-16">{item.name}</p>
            <p className="text-xl font-mono pt-5 mx-16">Narxi: {item.price}$</p>
            <p className="text-xl font-mono pt-5 mx-16">Hudud: {item.viloyat}</p>
            <p className="text-xl font-mono pt-5 mx-16">Yil: {item.yil}</p>
            <p className="text-xl font-mono pt-5 mx-16">Kuzov: {item.kuzov}</p>
            <p className="text-xl font-mono pt-5 mx-16">Rangi: {item.rang}</p>
            <p className="text-xl font-mono pt-5 mx-16">Uzatish: {item.uzatish}</p>
            <p className="text-xl font-mono pt-5 mx-16">Dvigatel hajmi: "{item.dvigatel}"</p>
            <button className="bg-green-500 rounded-md text-white text-xl px-4 py-2 mt-5 font-mono mx-16">{item.phone}</button>
            <button className="bg-yellow-500 rounded-md text-black text-sm px-9 py-2 mt-5 font-mono mx-16 mb-10">Xabar yuborish</button>
            </div>

            <div className='w-1/2 h-screen'>
            <p className="w-96 font-sans text-xl pt-10 mx-16">{item.decription}</p>
            <p className="w-72 font-sans text-xl pt-10 mx-16 pb-10">{item.decription2}</p>
            </div>
        </div>   
      ))}
    </div>
  )
}
export default OverViewPage;