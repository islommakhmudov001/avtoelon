import React from "react";


function Footer() {
  return (
    <div className="w-full flex flex-col items-center">
        <div className="w-full flex justify-center bg-[#F0F0F0]">
        <div className="container h-12 flex justify-evenly items-center">
            <div className="w-full flex justify-evenly">
              <p className="text-sm hover:text-red-500 text-blue-500 cursor-pointer">Sayt Xaritasi</p>
              <p className="text-sm hover:text-red-500 text-blue-500 cursor-pointer">Avtoelon.uz adminiga yozish </p>
              <p className="text-sm hover:text-red-500 text-blue-500 cursor-pointer">Shaxsiy kabinet</p>
              <p className="text-sm hover:text-red-500 text-blue-500 cursor-pointer">Sotish</p>
              <p className="text-sm hover:text-red-500 text-blue-500 cursor-pointer">E'lonni joylashtirish qoidalari</p>
            </div>
        </div>
        </div>
        <div className="w-full bg-[#1C1819]">
        <div className="container h-60 flex justify-evenly">
            <div className="w-5/12 flex flex-col items-center">
                <p className="pt-6 text-gray-400 text-sm">© 2023 MCHJ «NEWELD»</p>
                <p className="pt-3 text-white text-sm hover:underline cursor-pointer">Sayt haqida ma'lumot</p>
                <p className="pt-3 text-white text-sm hover:underline cursor-pointer">Foydalanuvchi shartnomasi</p>
                <p className="pt-3 text-white text-sm hover:underline cursor-pointer">Reklama beruvchilarga bag'ishlangan</p>
            </div>
            <div className="w-2/6">
                <p className="pt-6 text-white text-sm hover:underline cursor-pointer">Saytning mobil versiyasi</p>
                <p className="pt-3 text-white text-sm">Avtoelon.uz mobil ilovsini o'rnating</p>
            </div>
            <div className="w-2/6">
                <p className="pt-6 text-white text-sm">
                    Yangiliklar bilan tanishib boring!
                </p>
            </div>
        </div>
        </div>
    </div>
  )
}
export default Footer;