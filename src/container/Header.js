import { useContext, useEffect, useState } from "react";
import Input from "../inputs/input";
import { Context } from "../context";
import { Link } from "react-router-dom";

function Header () {
    const [value, setValue] = useState('')
    const {state, dispatch} = useContext(Context)

    const handleChanger = (e) =>{
        setValue(e.target.value)
    }
    const All = () => {
        setValue(' ')
    }
    useEffect(() => {
        dispatch({type: 'FILTER', payload: value})
    }, [value])

    return (
        <div className="w-full flex justify-center items-center">
        <div className='container h-20 flex items-center pt-4 justify-between px-9 pb-5'>
        <div className='w-1/3'>
          <Input/>
        </div>
        <div className="">
        <label htmlFor="meal" className='mr-2 font-medium dark:text-white'>Avtomobilni tanlang!</label>
        <select className="border-[1px] border-gray-500 outline-none" name="meal" id="meal" onClick={handleChanger}>
          <option value="" onClick={All}>Hammasi</option>
          <option value="uzb">Uzbekistan</option>
          <option value="yevro">Yevropa</option>
          <option value="china">China</option>
        </select>
        </div>

        </div>
        </div>
    )
}

export default Header