import {createContext, useReducer} from "react";
import { data } from "../data/data";

const initialValue = {
  data: data,
  search: '',
  filter: '',
  product: []
}

export const Context = createContext()


const reducer = (state = initialValue, action) => {
  const {type, payload} = action
  switch (type) {
    case "SEARCH":
      return {...state, search: payload}
    case "FILTER" :
      return {...state, filter: payload}
      case "PRODUCT" :
      const productAll = [...state.data.map(users =>
        users.id === payload ? {...users, comment: !users.comment} : {...users}
      )]
      return {...state, product: productAll.filter(person => person.comment !== false)}
    default:
      return {state}
  }
}

const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialValue)

  return <Context.Provider value={{state, dispatch}}>{children}</Context.Provider>
}

export default Provider