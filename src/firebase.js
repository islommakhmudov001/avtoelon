import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";


const firebaseConfig = {
  apiKey: "AIzaSyBEx0M0TOJLGEM72gVTQgw1G0alohOAhr0",
  authDomain: "avtoelon-41e0d.firebaseapp.com",
  projectId: "avtoelon-41e0d",
  storageBucket: "avtoelon-41e0d.appspot.com",
  messagingSenderId: "241733271112",
  appId: "1:241733271112:web:6e819278713952c1e3227d"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth()