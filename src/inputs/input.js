import { useContext, useEffect, useState } from "react";
import { Context } from "../context";

function Input () {
    const [value, setValue] = useState("")
    const {dispatch} = useContext(Context)


    useEffect(() => {
        dispatch({type: "SEARCH", payload: value})
    }, [value])

    return(
        <div>
          <input
            onChange={(e) => setValue(e.target.value)}
            value={value}
            type="text"
            className='py-1 px-8 text-lg rounded-2xl leading-6 outline-none border-[1px] border-gray-500'
            placeholder='Qidirish...'
          />
        </div>
    )

}
export default Input